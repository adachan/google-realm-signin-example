package com.nekologic.realmsigninexample

import android.util.Log
import io.realm.mongodb.App
import io.realm.mongodb.Credentials
import io.realm.mongodb.auth.GoogleAuthType
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlin.coroutines.suspendCoroutine

class MongoProxy {

    val app: App = App(RealmConsts.appId.value)

    suspend fun signInGoogleAsync (token: String): io.realm.mongodb.User? {
        val credentials = Credentials.google(token, GoogleAuthType.ID_TOKEN)
        return signInAsync(credentials)
    }

    suspend fun signInAsync(credentials: Credentials): io.realm.mongodb.User {
        return suspendCoroutine { cont ->
            app.loginAsync(credentials) { result ->
                if (result.isSuccess) cont.resumeWith(Result.success(result.get()))
                else cont.resumeWith(Result.failure(result.error))
            }
        }
    }

    suspend fun signInGoogle (token: String): io.realm.mongodb.User? {
        val credentials = Credentials.google(token, GoogleAuthType.ID_TOKEN)
        val result = signIn(credentials)
        return result.getOrNull()
    }

    suspend fun signIn(credentials: Credentials): Result<io.realm.mongodb.User?> {

        return withContext(Dispatchers.IO) {
            try {
                val user = app.login(credentials)
                Result.success(user)
            } catch (err: java.lang.Exception) {
                Log.d("AUTH", err.localizedMessage ?: "Unknown error")
                Result.failure(err)
            }
        }
    }
}