package com.nekologic.realmsigninexample

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import io.realm.mongodb.User
import kotlinx.coroutines.launch

class UserViewModel: ViewModel() {

    private val _user: MutableLiveData<User> = MutableLiveData()
    val user: LiveData<User?> = _user

    fun signInUser (account: GoogleSignInAccount?) {

        viewModelScope.launch {

            val token = account?.idToken
            if (token != null) {
                _user.value = App.mongoProxy.signInGoogle(token) // using app.login
                // _user.value = App.mongoProxy.signInGoogleAsync(token) // using app.loginAsync
            } else {
                Log.d("UserViewModel", "Could not get token $account")
                _user.value = null
            }
        }
    }
}