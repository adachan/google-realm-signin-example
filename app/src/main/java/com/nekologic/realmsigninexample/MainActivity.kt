package com.nekologic.realmsigninexample

import android.content.Intent
import android.os.Bundle
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment
import com.google.android.gms.auth.api.signin.*
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.nekologic.realmsigninexample.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private lateinit var userViewModel: UserViewModel
    private lateinit var googleSignInClient: GoogleSignInClient
    private lateinit var resultLauncher: ActivityResultLauncher<Intent>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)

        val uvm: UserViewModel by viewModels()
        userViewModel = uvm
        userViewModel.user.observe(this) {
            App.shared.toastIt("User updated: ${it}")
            navTo(R.id.readyFragment)
        }

        initGoogleSignIn()
    }

    private fun initGoogleSignIn() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(RealmConsts.clientIdWeb.value)
            .build()
        googleSignInClient = GoogleSignIn.getClient(this, gso)
        resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            val task: Task<GoogleSignInAccount> =
                GoogleSignIn.getSignedInAccountFromIntent(result.data)
            handleSignInResult(task)
        }
    }

    fun startRealmSignIn() {
        resultLauncher.launch(googleSignInClient.signInIntent)
        navTo(R.id.loadingFragment)
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            if (completedTask.isSuccessful) {
                val account: GoogleSignInAccount = completedTask.getResult(ApiException::class.java)
                App.shared.toastIt("Google Sign in OK, Signing into Realm", false)
                userViewModel.signInUser(account)
            } else {
                App.shared.toastIt("Google Auth failed: ${completedTask.exception.toString()}")
            }
        } catch (e: ApiException) {
            App.shared.toastIt("Google OAuth Error: ${e.message} ${GoogleSignInStatusCodes.getStatusCodeString(e.statusCode)}")
        }
    }

    private fun navTo (@IdRes fragId: Int) {
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment_content_main) as NavHostFragment
        val navController = navHostFragment.navController
        navController.navigate(fragId)
    }
}