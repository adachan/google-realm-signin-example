package com.nekologic.realmsigninexample

import android.app.Application
import android.content.res.Resources
import android.util.Log
import android.widget.Toast
import io.realm.Realm
import io.realm.RealmConfiguration

class App: Application() {
    override fun onCreate() {
        super.onCreate()

        shared = this
        Realm.init(this)
        val config = RealmConfiguration
            .Builder()
            .deleteRealmIfMigrationNeeded()
            .build()
        Realm.setDefaultConfiguration(config)

        appResources = resources
        mongoProxy = MongoProxy()
    }

    fun toastIt (value: String, isError: Boolean = true) {
        if (isError) Log.e("AUTH", value)
        Toast.makeText(this, value, Toast.LENGTH_SHORT).show()
    }

    companion object {
        lateinit var appResources: Resources
            private set
        lateinit var mongoProxy: MongoProxy
            private set
        lateinit var shared: App
            private set
    }
}