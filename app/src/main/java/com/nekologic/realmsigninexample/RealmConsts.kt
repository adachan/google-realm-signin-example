package com.nekologic.realmsigninexample

enum class RealmConsts(val value: String) {
    appId("YOUR-REALM-APP-ID"),
    clientIdWeb("YOUR-GOOGLE-CLOUD-PLATFORM-WEB-APP-CLIENT-ID")
}