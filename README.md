# Google+Realm SignIn Example

This project contains an Android App that has a basic Google Auth and Realm setup.

## Getting started

Clone the repo, then open it in Android Studio. Wait for the Gradle sync to complete, and make sure there are no build errors.  
You will need to replace the values held in the `RealmConsts.kt` file with your own values.
You should also change the package name, by right clicking on the package folder in the Project hierarchy, and selecting Refactor->Rename(Shift-f6). You will need this value in order to set up the GCP credential.

Set up a Realm Application as here:  
https://docs.mongodb.com/realm/manage-apps/create/create-with-realm-ui/

Once that's done, you will have a Realm App Id, which you can put into `RealmConsts.appId`

Then, you will need to set up a Google Cloud Platform app for the Google Sign In, using this guide:  
https://docs.mongodb.com/realm/authentication/google/

In the Google Cloud Project you will need to create:
* An Android OAuth credential, using your app's package id, and SHA1 key. The easiest way to generate your SHA1 key is to use `gradlew signingReport` from the terminal inside Android Studio.
* A Web Application OAuth credential. This is the clientId that you will use for `RealmConsts.clientWebId`
